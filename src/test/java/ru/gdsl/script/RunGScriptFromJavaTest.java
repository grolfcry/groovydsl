package ru.gdsl.script;

import groovy.lang.GroovyShell;
import org.junit.Test;

import java.io.File;

/**
 * User: fmv
 * Date: 29.05.12
 * Time: 20:11
 */
public class RunGScriptFromJavaTest {
    @Test
    public void run() throws Exception {
        GroovyShell shell = new GroovyShell();
        File f = new File(RunGScriptFromJavaTest.class.getResource("/GroovyTest.groovy").toURI());
        Object result = shell.evaluate(f);
        System.out.println("result=" + result);
    }
}
