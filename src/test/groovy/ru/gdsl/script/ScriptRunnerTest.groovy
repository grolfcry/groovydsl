package ru.gdsl.script

import org.junit.Test
import groovy.util.logging.Slf4j
import ru.gdsl.core.Archive
import groovy.mock.interceptor.StubFor

/**
 *
 * User: fmv
 * Date: 29.05.12
 * Time: 18:17
 *
 */
@Slf4j
class ScriptRunnerTest {
    final shouldFail = new GroovyTestCase().&shouldFail


    def mockedMethods = {
                check {String type -> true}
                archive {String sourcePath, String destPath, Boolean deleteSource ->}
                unArchive {String archiveName, String destPath, Boolean overwrite ->}
                sendMail {String to, String content ->}
    }

    @Test
    void runGood() {
        log.info "runGood"
        runScript()
    }
    @Test
    void runGood2() {
        log.info "runGood2"
        runScript("/ScenarioGood2.groovy")
    }
    @Test
    void runWithMockedScriptMethodsByMeta(){
        log.info "runWithMockedScriptMethods"
        ArchiveScript.metaClass.with {
            check{t->true}
            archive{params->}
            unArchive{params->}
            sendMail{params->}
        }
        runScript()
    }
    @Test
    void runWithMockedJavaMethodsByMeta(){
        log.info "runWithMockedScriptMethods"
        Archive.metaClass.with(mockedMethods)
        runScript()
    }

    @Test
    void runWithMockedJavaMethodsByStubFor(){
        log.info "runWithMockedJavaMethodsByStubFor"
        StubFor stub = new StubFor(Archive);
        stub.demand.with{
            mockedMethods
        }
        stub.use{
            runScript()
        }
    }

    @Test
    void runBad() {
        log.info "runBad"
        def err = shouldFail(MissingMethodException) {
            runScript "/ScenarioBad.groovy"
        }
        log.info err
    }

    private void runScript(scriptName = "/ScenarioGood.groovy") {
        ScriptRunner r = new ScriptRunner()
        File f = new File(ScriptRunnerTest.getResource(scriptName).toURI())
        r.runScript f, null
    }

}
