package ru.gdsl.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: fmv
 * Date: 29.05.12
 * Time: 16:58
 */
public class Archive {
    final Logger log = LoggerFactory.getLogger(this.getClass());
    public boolean check(String type) {
        log.info("check");
        if (type != null && type.toUpperCase().equals("ZIP"))
            return true;
        return false;
    }
    public void archive(String sourcePath,String destPath, Boolean deleteSource){
        log.info("archive");
    }

    public boolean unArchive(String archiveName, String destPath, Boolean overwrite){
        log.info("unArchive");
        return true;
    }

    public void sendMail(String to, String content){
        log.info("sendMail");
    }

}
