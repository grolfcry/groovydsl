package ru.gdsl.script

import ru.gdsl.core.Archive
import groovy.util.logging.Slf4j

/**
 * 
 * User: fmv
 * Date: 29.05.12
 * Time: 17:41
 * 
 */
@Slf4j
abstract class ArchiveScript extends Script{
    Archive arc = new Archive()
    def check(type = "ZIP"){
        log.info "check: ${type}"
        arc.check type
    }
    def archive(params){
        log.info "archive: ${params}"
        arc.archive params.sourcePath,params.destPath, params.deleteSource
    }
    def unArchive(params){
        log.info "unArchive: ${params}"
        arc.unArchive params.archiveName,params.destPath, params.overwrite
    }

    def sendMail(params){
        log.info "sendMail: ${params}"
        arc.sendMail params.to, params.content
    }
}
