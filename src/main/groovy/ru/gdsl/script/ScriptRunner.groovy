package ru.gdsl.script

import org.codehaus.groovy.control.CompilerConfiguration
import groovy.util.logging.Slf4j
import ru.gdsl.core.Archive

/**
 *
 * User: fmv
 * Date: 29.05.12
 * Time: 17:50
 *
 */
@Slf4j
class ScriptRunner {
    def runScript(String scriptName, String properties = null) {
        File f = new File(scriptName)
        runScript(f, properties);
    }

    def runScript(File f, String properties, baseScriptName = "ru.gdsl.script.ArchiveScript") {
        CompilerConfiguration conf = new CompilerConfiguration(scriptBaseClass: baseScriptName)
        GroovyShell shell = new GroovyShell(createBinding(), conf);
        Object result = shell.evaluate(f)
        log.info "result=" + result
    }

    private Binding createBinding(String properties) {
        Binding binding = new Binding()
        if (!properties)
            return binding
        def props = new Properties()
        new File(properties).withInputStream {
            stream -> props.load(stream)
        }
        props.entrySet().each {e ->
            binding.setVariable e.key, e.value
        }
        binding
    }

    static void main(String[] args) {
        ScriptRunner r = new ScriptRunner()
        if (args[0] == '-check') {
            println "check script"
            Archive.metaClass.with {
                check {String type ->
                    this.log.info "mock"
                    true
                }
                archive {String sourcePath, String destPath, Boolean deleteSource ->this.log.info "mock"}
                unArchive {String archiveName, String destPath, Boolean overwrite ->this.log.info "mock"}
                sendMail {String to, String content ->this.log.info "mock"}
            }
            r.runScript args[1]
            println "OK!"
        }
        else
            r.runScript args[0]
    }
}
